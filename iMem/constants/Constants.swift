//
//  Constants.swift
//  iMem
//
//  Created by Catalin Vescan on 17/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation

enum Storyboard: String {
    case Main
}

enum ControllerIdentifier: String {
    case ReminderController
    case ReminderDetails
    case AddReminder
    case MapViewController
    case CalendarController
    case PopoverViewController
    case CalendarReminders
}

enum KeychainKey: String {
    case passwordKye = "please give me my password"
    case isLoggedIn
}

enum UserDefaultsKey: String {
    case usernameKey = "this is my username"
}

struct AlertMessages {
    static let usernameRequired: String = "Username required"
    static let touchIDNotAvailable: String = "Touch ID not available"
    static let touchIDFailed: String = "Touch ID authentication failed"
    static let authWithTouchID: String = "Authenticate with Touch ID"
    static let userExistsWarning: String = "This user already exists"
    static let completeAllFields: String = "You need to complete all the fields"
    static let accessDenied: String = "Access denied"
    static let unknownStatus: String = "Unknown status"
    static let requestLocationDenied: String = "Request location was denied"
    static let somethingWentWrong: String = "Something went wrong"
    static let thisUserDoesntExists: String = "This user doesn't exists"
    static let call: String = "Call "
    static let calendarExists: String = "This calendar already exists"
}

struct AlertActionTitle {
    static let ok: String = "OK"
    static let call: String = "Call"
    static let cancel: String = "Cancel"
}

struct PickerConstants {
    static let months: [String] = DateFormatter().shortMonthSymbols
    static let days: [String] = DateFormatter().shortWeekdaySymbols
    static let hours: [Int] = Array(0...23)
    static let minutes: [Int] = Array(0...59)
}

struct DateFormat {
    static let notificationDateFormat: String = "Y-MM-dd HH:mm"
}

struct EntityName {
    static let user: String = "User"
    static let reminder: String = "Reminder"
    static let calendar: String = "Calendar"
}

struct NSCodingKey {
    static let id: String = "id"
    static let name: String = "name"
    static let phone: String = "phone"
    static let latitude: String = "latitude"
    static let longitute: String = "longitude"
}
