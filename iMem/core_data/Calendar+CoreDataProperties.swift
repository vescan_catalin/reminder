//
//  Calendar+CoreDataProperties.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//
//

import Foundation
import CoreData


extension Calendar {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Calendar> {
        return NSFetchRequest<Calendar>(entityName: "Calendar")
    }

    @NSManaged public var selected: Bool
    @NSManaged public var title: String?
    @NSManaged public var user: User?
    @NSManaged public var reminders: NSSet?

}

// MARK: Generated accessors for reminders
extension Calendar {

    @objc(addRemindersObject:)
    @NSManaged public func addToReminders(_ value: Reminder)

    @objc(removeRemindersObject:)
    @NSManaged public func removeFromReminders(_ value: Reminder)

    @objc(addReminders:)
    @NSManaged public func addToReminders(_ values: NSSet)

    @objc(removeReminders:)
    @NSManaged public func removeFromReminders(_ values: NSSet)

}
