//
//  DatabaseAccess.swift
//  iMem
//
//  Created by Catalin Vescan on 12/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DatabaseAccess {
    
    var context: NSManagedObjectContext!
    
    static let sharedInstance: DatabaseAccess = DatabaseAccess()

    private init() {
        self.context = self.persistentContainer.viewContext
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "iMem")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    func fetchEntries(forEntityNamed name: String) -> [NSManagedObject] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: name)
        request.returnsObjectsAsFaults = false
        
        do {
            return try context.fetch(request) as? [NSManagedObject] ?? []
        } catch {
            print("Failed fetch")
            return []
        }
    }
    
    func deleteAllData(from entityName: String) {
        let nsFetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteAll = NSBatchDeleteRequest(fetchRequest: nsFetchReq)
        do {
            try context.execute(deleteAll)
        } catch {
            print("Deleting failed")
        }
        
    }
    
    func checkForUserExistence(byValue value: String, withPassword password: String?) -> User? {
        // check if user exists
        let fetchRequest: NSFetchRequest<User> = User.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%K = %@", argumentArray: [#keyPath(User.username), value])
        
        do {
            let user = try context.fetch(fetchRequest)
            if user.first?.password == password {
                return user.first ?? nil
            } else {
                return nil
            }
        } catch {
            print("This user doesn't exists")
            return nil
        }
        
    }
    
    func newUser() -> User? {
        if let entityDescription = NSEntityDescription.entity(forEntityName: EntityName.user, in: self.context) {
             return NSManagedObject(entity: entityDescription, insertInto: self.context) as? User
        } else {
            return nil
        }
    }
    
    func newReminder() -> Reminder? {
        if let entityDescription = NSEntityDescription.entity(forEntityName: EntityName.reminder, in: self.context) {
            return NSManagedObject(entity: entityDescription, insertInto: self.context) as? Reminder
        } else {
            return nil
        }
    }
    
    func newCalendar() -> Calendar? {
        if let entityDescription = NSEntityDescription.entity(forEntityName: EntityName.calendar, in: self.context) {
            return NSManagedObject(entity: entityDescription, insertInto: self.context) as? Calendar
        } else {
            return nil
        }
    }
}

