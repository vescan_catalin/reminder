//
//  Reminder+CoreDataProperties.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//
//

import Foundation
import CoreData


extension Reminder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Reminder> {
        return NSFetchRequest<Reminder>(entityName: "Reminder")
    }

    @NSManaged var contactPerson: ContactPerson?
    @NSManaged public var date: NSDate!
    @NSManaged var location: MyLocation?
    @NSManaged public var notificationTime: NSDate!
    @NSManaged public var title: String!
    @NSManaged public var calendar: Calendar?
    @NSManaged public var user: User?

}
