//
//  AddReminderViewController.swift
//  iMem
//
//  Created by Catalin Vescan on 17/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit
import ContactsUI
import CoreLocation
import CoreData
import UserNotifications

class AddReminderViewController: UIViewController {

    @IBOutlet weak var dateReminderPicker: UIDatePicker!
    @IBOutlet weak var timeNotificationPickerView: UIPickerView!
    @IBOutlet weak var contactPersonPickerButton: UIButton!
    @IBOutlet weak var titleReminderTextField: UITextField!
    @IBOutlet weak var locationButton: UIButton!
    
    private let databaseAccess: DatabaseAccess = DatabaseAccess.sharedInstance
    private var fetchedResultsController: NSFetchedResultsController<Calendar>!
    
    var user: User!
    var calendar: Calendar?
    var reminderTitle: String?
    var contact: ContactPerson?
    var location: MyLocation?
    var selectedDate: NSDate?
    var notificationTime: TimeNotification = TimeNotification()
    var newCalendar: Calendar?
    
    lazy var dateFormatter: DateFormatter = {
        return DateFormatter.dateFormatter()
    }()
    lazy var date: Date = {
        let today = Date()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: today)
        
        if let dateToReturn = self.dateFormatter.calendar.date(from: components){
            return dateToReturn
        } else {
            return Date()
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleReminderTextField.delegate = self
        self.timeNotificationPickerView.dataSource = self
        self.timeNotificationPickerView.delegate = self
        
        self.dateReminderPicker.datePickerMode = .dateAndTime
        self.dateReminderPicker.timeZone = NSTimeZone.init(forSecondsFromGMT: 0) as TimeZone
        self.dateReminderPicker.date = self.date
        self.dateReminderPicker.minimumDate = self.dateReminderPicker.date
        
        self.contactPersonPickerButton.isEnabled = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "New Reminder"
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(title: "Save",
                            style: .plain, target: self, action: #selector(save))
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissViewController))
        self.view.addGestureRecognizer(gesture)
        
        pickerInit()
    }
    
    @objc func dismissViewController() {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func pickerInit() {
        self.notificationTime.hour = PickerConstants.hours.first
        self.notificationTime.minute = PickerConstants.minutes.first
    }
    
    @IBAction func openMap(_ sender: Any) {
        if let mapViewController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier: ControllerIdentifier.MapViewController.rawValue) as? MapViewController,
            let navigator = self.navigationController {
            mapViewController.mapViewDelegate = self
            
            navigator.pushViewController(mapViewController, animated: true)
        }
    }
    
    @IBAction func datePicker(_ sender: Any) {
        self.selectedDate = self.dateReminderPicker.date as NSDate
    }
    
    @objc func save() {
        self.reminderTitle = self.titleReminderTextField.text
        
        if self.selectedDate == nil {
            self.selectedDate = self.dateReminderPicker.date as NSDate
        }
        
        self.notificationTime.date = self.selectedDate
        
        if self.calendar == nil {
            guard let calendarForOtherReminders = fetchOthersCalendar() else {
                return
            }
            
            calendarForOtherReminders.user = self.user
            calendarForOtherReminders.selected = false
            self.calendar = calendarForOtherReminders
        }
        
        if let remTitle = self.reminderTitle, let date = self.selectedDate,
            let notifTime = self.notificationTime.reminderTime() {
            // save new reminder in db
            if let newReminder = self.databaseAccess.newReminder() {
                newReminder.contactPerson = self.contact
                newReminder.date = date
                newReminder.location = self.location
                newReminder.notificationTime = notifTime
                newReminder.title = remTitle
                newReminder.calendar = self.calendar
                newReminder.user = self.user
                
                self.databaseAccess.saveContext()
            } else {
                self.present(UIAlertController
                    .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                         AlertMessages.somethingWentWrong, .alert, nil), animated: true)
                return
            }
        }
        
        guard let reminderTitle = self.reminderTitle, let eventTime = self.selectedDate as Date? else {
            return
        }
        
        scheduleNotification(forReminder: reminderTitle, at: eventTime, before: self.notificationTime)
        
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { _ in
            self.navigationController?.popViewController(animated: true)
        }
    }
   
    func fetchOthersCalendar() -> Calendar? {
        let fetchRequest: NSFetchRequest<Calendar> = Calendar.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "title == %@", "Others")
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest:
            fetchRequest, managedObjectContext: self.databaseAccess.context,
                          sectionNameKeyPath: nil, cacheName: nil)
        
        self.fetchedResultsController.delegate = self
        do {
            let calendars = try self.databaseAccess.context.fetch(fetchRequest)
            if let calendar = calendars.first {
                return calendar
            } else {
                if let otherCalendar = self.databaseAccess.newCalendar() {
                    otherCalendar.title = "Others"
                    
                    return otherCalendar
                }
            }
        } catch {
            print("Could not fetch data")
        }
        
        return nil
    }
    
    @IBAction @objc func chooseContact() {
        let contactViewController = CNContactPickerViewController()
        contactViewController.delegate = self
        
        let store = CNContactStore()
        
        switch CNContactStore.authorizationStatus(for: .contacts) {
            case .notDetermined:
                store.requestAccess(for: .contacts, completionHandler: {
                    access, error in
                    DispatchQueue.main.async {
                        if access {
                            self.present(contactViewController, animated: true, completion: nil)
                        } else {
                            self.present(UIAlertController
                                .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                                     AlertMessages.accessDenied, .alert, nil), animated: true)
                        }
                    }
                })
            case .authorized:
                self.present(contactViewController, animated: true, completion: nil)
            case .denied:
                self.present(UIAlertController
                    .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                         AlertMessages.accessDenied, .alert, nil), animated: true)
            default:
                self.present(UIAlertController
                    .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                         AlertMessages.unknownStatus, .alert, nil), animated: true)
        }
    }
    
    func scheduleNotification(forReminder reminder: String, at eventTime: Date, before notificationTime: TimeNotification) {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {
            didAllow, error in
            if !didAllow {
                print("user has declined notifications")
            }
        })
        notificationCenter.getNotificationSettings(completionHandler: {
            settings in
            if settings.authorizationStatus != .authorized {
                print("notification not allowed")
            }
        })
        
        guard let timeOfTheEvent = self.dateFormatter.string(from: eventTime).split(separator: " ").last else {
            return
        }
        
        let content = UNMutableNotificationContent()
        content.title = reminder
        content.body = "You have an event today at " + timeOfTheEvent
        content.sound = UNNotificationSound.default
        content.badge = 1
       
        var components = self.dateFormatter.calendar.dateComponents([.year, .month, .day, .hour, .minute], from: eventTime)
        if let hour = components.hour, let minute = components.minute {
            components.hour = hour - (notificationTime.hour ?? 0)
            components.minute = minute - (notificationTime.minute ?? 0)
        }
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        
        let identifier = "LocalNotification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request, withCompletionHandler: {
            error in
            
            guard error == nil else {
                return
            }
            
            print("Notification scheduled!")
        })
    }
    
}

extension AddReminderViewController: MapViewDelegate, NSFetchedResultsControllerDelegate {
    func getLocationTouched(latitude: Double, longitude: Double) {
        self.location = MyLocation(latitude: latitude, longitude: longitude)
    }
}

extension AddReminderViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return false
    }
}

extension AddReminderViewController: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        let contactIdentifier = contact.identifier
        let contactName = contact.givenName + " " + contact.familyName
        guard let contactPhone = contact.phoneNumbers.first?.value.stringValue else {
            return
        }
        
        self.contact = ContactPerson(id: contactIdentifier, name: contactName, phone: contactPhone)
        
        self.contactPersonPickerButton.isEnabled = false
        self.contactPersonPickerButton.setTitle(contactName + " " + contactPhone, for: .normal)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddReminderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let width = view.frame.size.width
        switch component {
            case 1:
                return width / 6
            case 2:
                return width / 3
            default:
                return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
            case 1:
                return PickerConstants.hours.count
            case 2:
                return PickerConstants.minutes.count
            default:
                return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
            case 1:
                return "\(PickerConstants.hours[row]) h"
            case 2:
                return "\(PickerConstants.minutes[row]) min"
            default:
                return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
            case 1:
                self.notificationTime.hour = PickerConstants.hours[row]
            case 2:
                self.notificationTime.minute = PickerConstants.minutes[row]
            default:
                return
        }
    }
}
