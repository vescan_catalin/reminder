//
//  CalendarRemindersViewController.swift
//  iMem
//
//  Created by Catalin Vescan on 01/10/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit
import CoreData

class CalendarRemindersViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    private let databaseAccess: DatabaseAccess = DatabaseAccess.sharedInstance
    private var fetchedResultsController: NSFetchedResultsController<Reminder>!
    
    var user: User!
    var calendar: Calendar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ReminderCell", bundle: nil),
                                forCellReuseIdentifier: "ReminderCell")
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissViewController))
        self.tableView.addGestureRecognizer(gesture)
        
        fetchReminders()
        setupUI()
    }
    
    @objc func dismissViewController() {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func fetchReminders() {
        let fetchRequest: NSFetchRequest<Reminder> = Reminder.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "calendar == %@", self.calendar)
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest:
            fetchRequest, managedObjectContext: self.databaseAccess.context,
                          sectionNameKeyPath: nil, cacheName: nil)
        
        self.fetchedResultsController.delegate = self
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("Could not fetch data")
        }
    }
    
    func setupUI() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.title = "Calendar Reminders"
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewReminder))
    }
    
    @objc func addNewReminder() {
        if let addReminderController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier:
                ControllerIdentifier.AddReminder.rawValue) as? AddReminderViewController,
            let navigator = self.navigationController {
            addReminderController.user = user
            addReminderController.calendar = self.calendar
            
            navigator.pushViewController(addReminderController, animated: true)
        }
    }
}

extension CalendarRemindersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderCell", for: indexPath) as? ReminderCell else {
            return UITableViewCell()
        }
        
        let reminder: Reminder = self.fetchedResultsController.object(at: indexPath)
        let date = DateFormatter.localizedString(from: reminder.date as Date, dateStyle: .medium, timeStyle: .none)
        
        if let title = reminder.title {
            cell.reminderName.text = title
            cell.reminderDate.text = date
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let reminder: Reminder = self.fetchedResultsController.object(at: indexPath)
        if let reminderDetailsController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier: ControllerIdentifier.ReminderDetails.rawValue) as? ReminderDetailsViewController,
            let navigator = self.navigationController {
            reminderDetailsController.reminder = reminder

            navigator.pushViewController(reminderDetailsController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let reminder: Reminder = self.fetchedResultsController.object(at: indexPath)
            self.databaseAccess.context.delete(reminder)
            do{
                try self.databaseAccess.context.save()
            } catch {
            }
        }
    }
}

extension CalendarRemindersViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.reloadData()
    }
}
