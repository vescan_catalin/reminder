//
//  CalendarViewController.swift
//  iMem
//
//  Created by Catalin Vescan on 25/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//
import UIKit
import CoreData

class CalendarViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!    
    @IBOutlet weak var headerView: UIView!
    
    private let databaseAccess: DatabaseAccess = DatabaseAccess.sharedInstance
    private var fetchedResultsController: NSFetchedResultsController<Calendar>!
    
    var user: User!
    var calendars: [Calendar]!
    var selectedCells = [IndexPath] ()
    var filteredCalendars: [Calendar] = [Calendar]()
    
    var addCalendarButton: UIBarButtonItem!
    var deleteCalendarCell: UIBarButtonItem!
    var editCalendarCell: UIBarButtonItem!
    var editableMode: Bool = false
    var collectionViewCellSize: CGSize!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "CalendarCell", bundle: nil),
                                     forCellWithReuseIdentifier: "CalendarCell")
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(dismissViewController))
        self.collectionView.addGestureRecognizer(gesture)
        
        fetchCalendars()
        searchBarSetup()
    }
    
    @objc func dismissViewController() {
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func fetchCalendars() {
        let fetchRequest: NSFetchRequest<Calendar> = Calendar.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "user == %@", self.user)
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest:
            fetchRequest, managedObjectContext: self.databaseAccess.context,
                          sectionNameKeyPath: nil, cacheName: nil)
        
        self.fetchedResultsController.delegate = self
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("Could not fetch data")
        }
        self.calendars = self.fetchedResultsController.fetchedObjects
    }
    
    func searchBarSetup() {
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search here ..."
        self.headerView.addSubview(self.searchController.searchBar)
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.definesPresentationContext = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.title = "Calendars"
        self.addCalendarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self,
                                                action: #selector(addNewCalendar))
        self.editCalendarCell = UIBarButtonItem(barButtonSystemItem: .edit, target: self,
                                                action: #selector(editCalendarCells))
        self.deleteCalendarCell = UIBarButtonItem(title: "Delete", style: .plain, target: self,
                                                  action: #selector(deleteCalendarCells))
        
        self.navigationItem.rightBarButtonItems = [addCalendarButton, editCalendarCell]
    }
    
    @objc func editCalendarCells() {
        self.navigationItem.rightBarButtonItems?[1] = self.deleteCalendarCell
        self.editableMode = true
    }
    
    @objc func deleteCalendarCells() {
        self.navigationItem.rightBarButtonItems?[1] = self.editCalendarCell
        
        let indexPaths = self.selectedCells.reversed().map{ $0 }
        indexPaths.forEach({
            indexPath in
            let calendar: Calendar = self.fetchedResultsController.object(at: indexPath)
            if let index = indexPath.first {
                self.selectedCells.remove(at: index)
                self.databaseAccess.context.delete(calendar)
            }
        })
        do{
            try self.databaseAccess.context.save()
        } catch {
        }
        
        self.selectedCells.removeAll()
        self.editableMode = false
    }
    
    @objc func addNewCalendar() {
        guard let popoverController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier:
                ControllerIdentifier.PopoverViewController.rawValue) as? PopoverViewController else {
                return
        }
        
        popoverController.modalPresentationStyle = .popover
        if let popover = popoverController.popoverPresentationController {
            popover.barButtonItem = self.navigationItem.rightBarButtonItem
            popover.delegate = self
            popoverController.popoverVCDelegate = self
            
            self.present(popoverController, animated: true)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator:
        UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        self.collectionViewCellSize = UIScreen.main.bounds.size
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !self.searchController.isActive {
           self.searchController.searchBar.frame = CGRect(x: 0, y: 0, width:
            self.headerView.frame.width, height: 50)
        }
    }
}

extension CalendarViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchResults = searchController.searchBar.text else {
            return
        }
        let pattern = "^\(searchResults).*".lowercased()
        if !searchResults.isEmpty {
            if self.calendars != nil {
                self.filteredCalendars = self.fetchedResultsController.fetchedObjects?.filter({
                    $0.title?.range(of: pattern, options: .regularExpression) != nil
                }) ?? []
            }
        } else {
            if self.calendars != nil {
                self.filteredCalendars = self.calendars
            }
        }
        
        self.collectionView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.addCalendarButton.isEnabled = false
        self.editCalendarCell.isEnabled = false
        self.deleteCalendarCell.isEnabled = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.addCalendarButton.isEnabled = true
        self.editCalendarCell.isEnabled = true
        self.deleteCalendarCell.isEnabled = true
    }
}

extension CalendarViewController: PopoverViewControllerDelegate {
    func createNewCalendar(title: String) {
        let calendar = self.fetchedResultsController.fetchedObjects?.first(where: {
            $0.title == title
        })
            
        if calendar == nil {
            if let otherCalendar = self.databaseAccess.newCalendar() {
                otherCalendar.title = title
                otherCalendar.user = self.user
                otherCalendar.selected = false
                
                self.databaseAccess.saveContext()
            }
        }
    }
}

extension CalendarViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension CalendarViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView.reloadData()
    }
}

extension CalendarViewController: UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.searchController.searchBar.text == nil ||
            !(self.searchController.searchBar.text?.isEmpty == true) {
            return self.filteredCalendars.count
        } else {
            return self.fetchedResultsController.fetchedObjects?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarCell",
                                                            for: indexPath) as? CalendarCell else {
            return UICollectionViewCell()
        }
        cell.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        
        if !self.filteredCalendars.isEmpty && self.searchController.searchBar.text != "" {
            cell.calendarTitle.text = self.filteredCalendars[indexPath.row].title
        } else {
            let calendar: Calendar = self.fetchedResultsController.object(at: indexPath)
            cell.calendarTitle.text = calendar.title
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.editableMode {
            if !self.selectedCells.contains(indexPath) {
                self.selectedCells.append(indexPath)
                if let cell = collectionView.cellForItem(at: indexPath) {
                    cell.layer.borderWidth = 3
                    cell.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
            } else {
                self.selectedCells.removeAll(where: { $0 == indexPath })
                if let cell = collectionView.cellForItem(at: indexPath) {
                    cell.layer.borderWidth = 0
                    cell.layer.borderColor = nil
                }
            }
        } else {
            if let calendarRemindersController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
                .instantiateViewController(withIdentifier:
                    ControllerIdentifier.CalendarReminders.rawValue) as? CalendarRemindersViewController,
                let navigator = self.navigationController {
                calendarRemindersController.user = self.user
                if self.searchController.searchBar.text != "" {
                    calendarRemindersController.calendar = self.filteredCalendars[indexPath.row]
                } else {
                    calendarRemindersController.calendar = self.fetchedResultsController.object(at: indexPath)
                }
                self.searchController.isActive = false
                
                navigator.pushViewController(calendarRemindersController, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameWidth = self.view.frame.size.width - 50
        
        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft ||
            UIDevice.current.orientation == UIDeviceOrientation.landscapeRight {
            return CGSize(width: frameWidth / 5, height: frameWidth / 5)
        }
        
        return CGSize(width: frameWidth / 3, height: frameWidth / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 25, left: 50, bottom: 25, right: 50)
    }
    
    
}
