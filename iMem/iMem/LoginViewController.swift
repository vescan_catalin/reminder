//
//  ViewController.swift
//  iMem
//
//  Created by Alin Petrus on 9/10/19.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit
import CoreData
import LocalAuthentication
import KeychainSwift

class LoginViewController: UIViewController {
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var homeImage: UIImageView!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var registerButton: UIButton!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var touchIDButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private var device: String?
    private let databaseAccess: DatabaseAccess = DatabaseAccess.sharedInstance
    private let keychain = KeychainSwift()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(scrollviewAdjustment),
                                 name: UIResponder.keyboardWillHideNotification, object: nil)
        notification.addObserver(self, selector: #selector(scrollviewAdjustment),
                                 name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUIElements()
        
        if let isLoggedIn = self.keychain.getBool(KeychainKey.isLoggedIn.rawValue) {
            if isLoggedIn {
                self.keychainAuthentication()
            }
        }
        
    }
    
    func setUIElements() {
        self.homeImage.layer.cornerRadius = homeImage.frame.height / 2
        self.homeImage.clipsToBounds = true
        
        self.registerButton.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.registerButton.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.registerButton.layer.cornerRadius = 5
        self.registerButton.layer.borderWidth = 1
        self.registerButton.isEnabled = true
        
        self.loginButton.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.loginButton.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.loginButton.layer.cornerRadius = 5
        self.loginButton.layer.borderWidth = 1
        self.loginButton.isEnabled = true
        
        self.touchIDButton.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.touchIDButton.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.touchIDButton.layer.cornerRadius = 5
        self.touchIDButton.layer.borderWidth = 1
        self.touchIDButton.isEnabled = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func authenticateWithTouchId(_ sender: Any) {
        self.touchIDButton.isEnabled = false
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            let reason = AlertMessages.authWithTouchID
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, error in
                if success {
                    DispatchQueue.main.async {
                        self.keychain.set(true, forKey: KeychainKey.isLoggedIn.rawValue)
                        self.keychainAuthentication()
                    }
                } else {
                    self.present(UIAlertController
                        .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                             AlertMessages.touchIDFailed, .alert, nil), animated: true)
                }
            }
        } else {
            self.present(UIAlertController
                .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                     AlertMessages.touchIDNotAvailable, .alert, nil), animated: true)
        }
    }
    
    @IBAction func login(_ sender: Any) {
        guard let usernameText = self.usernameTextField.text,
            let passwordText = self.passwordTextField.text else {
                return
        }
        if let user = self.databaseAccess.checkForUserExistence(byValue: usernameText,
                                                                withPassword: passwordText){
            self.loginButton.isEnabled = false
            self.startActivityIndicatorAnimation()
            self.saveData(username: usernameText, password: passwordText)
            
            DispatchQueue.main.async {
                self.stopActivityIndicatorAnimation()
                
                if let reminderController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
                    .instantiateViewController(withIdentifier: ControllerIdentifier.ReminderController.rawValue) as? ReminderViewController,
                    let navigator = self.navigationController {
                    reminderController.user = user
                    navigator.pushViewController(reminderController, animated: true)
                }
            }
        } else {
            self.present(UIAlertController
                .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                     AlertMessages.thisUserDoesntExists, .alert, nil), animated: true)
        }
    }
    
    @IBAction func register(_ sender: Any) {
        guard self.usernameTextField.text != "", self.passwordTextField.text != "",
            let usernameText = self.usernameTextField.text,
            let passwordText = self.passwordTextField.text else {
                self.present(UIAlertController
                    .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                         AlertMessages.completeAllFields, .alert, nil), animated: true)
                
                return
        }
        
        // if user doesn't exists
        if self.databaseAccess.checkForUserExistence(byValue: usernameText, withPassword: passwordText) == nil {
            if let user = self.databaseAccess.newUser() {
                user.username = usernameText
                user.password = passwordText
                
                self.databaseAccess.saveContext()
            }
            DispatchQueue.main.async {
                // save username and password for auto login
                self.saveData(username: usernameText, password: passwordText)
                self.keychainAuthentication()
            }
        } else {
            self.present(UIAlertController
                .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                     AlertMessages.userExistsWarning, .alert, nil), animated: true)
        }
    }
    
    @objc func scrollviewAdjustment(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                let keyboardViewEndFrame = self.view.convert(keyboardScreenEndFrame, to: self.view.window)
                if notification.name == UIResponder.keyboardWillHideNotification {
                    self.scrollView.contentInset = UIEdgeInsets.zero
                } else {
                    self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
                }
                
                self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset
            }
        }
    }
    
    func startActivityIndicatorAnimation() {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
    }
    
    func stopActivityIndicatorAnimation() {
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
        self.view.isUserInteractionEnabled = true
    }
    
    func keychainAuthentication() {
        if let username = UserDefaults.standard.value(forKey: UserDefaultsKey.usernameKey.rawValue) as? String,
            let password = self.keychain.get(KeychainKey.passwordKye.rawValue){
            if let user = self.databaseAccess.checkForUserExistence(byValue: username, withPassword: password) {
                
                if let reminderController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
                    .instantiateViewController(withIdentifier: ControllerIdentifier.ReminderController.rawValue) as? ReminderViewController,
                    let navigator = self.navigationController {
                    reminderController.user = user
                    navigator.pushViewController(reminderController, animated: true)
                }
            } else {
                self.present(UIAlertController
                    .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                         AlertMessages.thisUserDoesntExists, .alert, nil), animated: true)
            }
        } else {
            self.present(UIAlertController
                .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                     AlertMessages.thisUserDoesntExists, .alert, nil), animated: true)
        }
    }
    
    func saveData(username: String, password: String) {
        self.keychain.set(password, forKey: KeychainKey.passwordKye.rawValue)
        self.keychain.set(true, forKey: KeychainKey.isLoggedIn.rawValue)
        UserDefaults.standard.set(username, forKey: UserDefaultsKey.usernameKey.rawValue)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return false
    }
}
