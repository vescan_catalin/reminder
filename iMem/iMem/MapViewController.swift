//
//  MapViewController.swift
//  iMem
//
//  Created by Catalin Vescan on 20/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewDelegate {
    func getLocationTouched(latitude: Double, longitude: Double)
}

class MapViewController: UIViewController {

    @IBOutlet private weak var mapView: MKMapView!
    private let annotation: MKPointAnnotation = MKPointAnnotation()
    private var locationManager: CLLocationManager = CLLocationManager()
    var mapViewDelegate: MapViewDelegate?
    weak var reminder: Reminder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.mapType = MKMapType.standard
        self.mapView.isZoomEnabled = true
        self.mapView.isScrollEnabled = true
        self.mapView.center = view.center
        self.mapView.isUserInteractionEnabled = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        mapView.addGestureRecognizer(gestureRecognizer)
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .denied:
                self.present(UIAlertController
                    .showAlertController(defaultAlertActionButton: AlertActionTitle.ok,
                                         AlertMessages.requestLocationDenied, .alert, nil), animated: true)
            default:
                break
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        if self.reminder != nil {
            if let latitude = self.reminder?.location?.latitude,
                let longitude = self.reminder?.location?.longitude {
                pinLocation(latitude: latitude, longitude: longitude)
            }
        }
    }
    
    @objc func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
        let location = gestureReconizer.location(in: self.mapView)
        let coordinate = self.mapView.convert(location, toCoordinateFrom: self.mapView)
        
        self.annotation.coordinate = coordinate
        self.mapView.addAnnotation(self.annotation)
        self.mapView.isUserInteractionEnabled = false
        
        if self.mapViewDelegate != nil {
            self.mapViewDelegate?.getLocationTouched(latitude: coordinate.latitude,
                                                     longitude: coordinate.longitude)
        }
        
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { _ in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func pinLocation(latitude: Double, longitude: Double) {
//        self.mapView.isUserInteractionEnabled = false
        
        let centerCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.annotation.coordinate = centerCoordinate
        self.annotation.title = "Hey there!🤪"
        self.mapView.addAnnotation(self.annotation)
        
        let mapCenter = centerCoordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: mapCenter, span: span)
        self.mapView.region = region
    }
}
