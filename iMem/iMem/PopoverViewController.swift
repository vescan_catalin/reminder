//
//  PopoverViewController.swift
//  iMem
//
//  Created by Catalin Vescan on 25/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit

protocol PopoverViewControllerDelegate {
    func createNewCalendar(title: String)
}

class PopoverViewController: UIViewController {

    @IBOutlet weak var popoverTextField: UITextField!
    @IBOutlet weak var popoverTitleLabel: UILabel!
    
    var popoverVCDelegate: PopoverViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.popoverTitleLabel.numberOfLines = 0
        self.popoverTextField.delegate = self
        self.popoverTextField.becomeFirstResponder()
    }
    
    @IBAction func popoverSaveButton(_ sender: Any) {
        if let title = self.popoverTextField.text, self.popoverTextField.text != "" {
            if self.popoverVCDelegate != nil {
                self.popoverVCDelegate?.createNewCalendar(title: title)
            }
            self.dismiss(animated: true, completion: nil)
        } else {
            self.present(UIAlertController.showAlertController(defaultAlertActionButton: AlertActionTitle.ok, AlertMessages.completeAllFields, .alert, nil), animated: true)
        }
    }
    
}

extension PopoverViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return true
    }
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}
