//
//  ReminderDetails.swift
//  iMem
//
//  Created by Catalin Vescan on 17/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit

class ReminderDetailsViewController: UIViewController {

    var reminder: Reminder!
    lazy var dateFormatter: DateFormatter = {
        return DateFormatter.dateFormatter()
    }()
    
    @IBOutlet weak var reminderTitleLabel: UILabel!
    @IBOutlet weak var calendarTitleReminderLabel: UILabel!
    @IBOutlet weak var callContactPersonButton: UIButton!
    @IBOutlet weak var viewLocationOnMapButton: UIButton!
    @IBOutlet weak var establishedEventDateLabel: UILabel!
    @IBOutlet weak var establishedNotificationTimeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUIElements()
    }
    
    func setUIElements() {
        guard let reminder = self.reminder else {
            return
        }
        
        var contactButtonTitle: String = "No contact to show"
        
        if reminder.contactPerson != nil {
            if let name = reminder.contactPerson?.name,
                let phone = reminder.contactPerson?.phone {
                contactButtonTitle = name + " " + phone
            }
        }
        
        let reminderDateTransformed = self.dateFormatter.string(from: reminder.date as Date)
        let notificationDateTransformed = self.dateFormatter.string(from: reminder.notificationTime as Date)
        
        self.reminderTitleLabel.text = reminder.title
        self.calendarTitleReminderLabel.text = reminder.calendar?.title
        self.callContactPersonButton.setTitle(contactButtonTitle, for: .normal)
        self.viewLocationOnMapButton.setTitle("View on map", for: .normal)
        self.establishedEventDateLabel.text = reminderDateTransformed
        self.establishedNotificationTimeLabel.text = notificationDateTransformed
    }
    
    @IBAction func openContact(_ sender: Any) {
        guard let phone = self.reminder.contactPerson?.phone,
            let phoneURL = URL(string: "tel://" + phone) else {
                return
        }
        
        self.present(UIAlertController
            .showAlertController(defaultAlertActionButton: AlertActionTitle.call, AlertMessages.call + phone + "?",
                                .actionSheet, { action in
                                    UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
            }), animated: true)
    }
    
    @IBAction func viewLocation(_ sender: Any) {
        if let mapViewController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier: ControllerIdentifier.MapViewController.rawValue) as? MapViewController,
            let navigator = self.navigationController {
            mapViewController.reminder = self.reminder
            navigator.pushViewController(mapViewController, animated: true)
        }
    }
    
}

