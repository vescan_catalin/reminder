//
//  OptionsController.swift
//  iMem
//
//  Created by Catalin Vescan on 12/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit
import CoreData
import KeychainSwift

class ReminderViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private let databaseAccess: DatabaseAccess = DatabaseAccess.sharedInstance
    private var fetchedResultsController: NSFetchedResultsController<Reminder>!
    private let keychain = KeychainSwift()
    
    var user: User!
    var reminders: [Reminder]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "ReminderCell", bundle: nil),
                                forCellReuseIdentifier: "ReminderCell")
        let fetchRequest: NSFetchRequest<Reminder> = Reminder.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "user == %@", self.user)
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.databaseAccess.context, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self

        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            print("Could not fetch data")
        }
        
        setNavigationBarItems()
    }
    
    func setNavigationBarItems() {
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout)), animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = "Reminders"
        
        let addReminderButton = UIBarButtonItem(barButtonSystemItem: .add, target: self,
                                                action: #selector(addNewReminder))
        let viewCalendarButton = UIBarButtonItem(title: "Calendar", style: .plain, target: self,
                                                 action: #selector(viewCalendar))
        self.navigationItem.rightBarButtonItems = [addReminderButton, viewCalendarButton]
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem =
            UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout))
    }
    
    @objc func addNewReminder() {
        if let addReminderController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier: ControllerIdentifier.AddReminder.rawValue) as?
            AddReminderViewController,
            let navigator = self.navigationController {
            addReminderController.user = user
            
            navigator.pushViewController(addReminderController, animated: true)
        }
    }
    
    @objc func logout() {
        self.keychain.set(false, forKey: KeychainKey.isLoggedIn.rawValue)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func viewCalendar() {
        if let calendarController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier: ControllerIdentifier.CalendarController.rawValue) as?
            CalendarViewController,
            let navigator = self.navigationController {
            calendarController.user = user
            
            navigator.pushViewController(calendarController, animated: true)
        }
    }
}

extension ReminderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderCell", for: indexPath) as?
            ReminderCell else {
                fatalError("Cell unregistered")
        }
        
        let reminder: Reminder = self.fetchedResultsController.object(at: indexPath)
        let date = DateFormatter.localizedString(from: reminder.date as Date, dateStyle: .medium, timeStyle: .none)
        
        if let title = reminder.title {
            cell.reminderName.text = title
            cell.reminderDate.text = date
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let reminder: Reminder = self.fetchedResultsController.object(at: indexPath)
        if let reminderDetailsViewController = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
            .instantiateViewController(withIdentifier: ControllerIdentifier.ReminderDetails.rawValue) as? ReminderDetailsViewController,
            let navigator = self.navigationController {
            reminderDetailsViewController.reminder = reminder

            navigator.pushViewController(reminderDetailsViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let reminder: Reminder = self.fetchedResultsController.object(at: indexPath)
            self.databaseAccess.context.delete(reminder)
            do{
                try self.databaseAccess.context.save()
            } catch {
            }
        }
    }
}

extension ReminderViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.reloadData()
    }
}

