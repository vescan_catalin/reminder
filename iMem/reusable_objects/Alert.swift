//
//  Alert.swift
//  iMem
//
//  Created by Catalin Vescan on 20/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit

extension UIAlertController {
    class func showAlertController(defaultAlertActionButton: String, _ message: String,
                                   _ style: UIAlertController.Style, _ handler: ((UIAlertAction) -> Void)?) -> UIAlertController {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: style)
        alertController.addAction(UIAlertAction(title: defaultAlertActionButton, style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: AlertActionTitle.cancel, style: .cancel, handler: nil))
        
        return alertController
    }
}
