//
//  ContactPerson.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation

@objc class ContactPerson: NSObject, Codable, NSCoding {
    var id: String
    var name: String?
    var phone: String?
    
    init(id: String, name: String, phone: String) {
        self.id = id
        self.name = name
        self.phone = phone
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: NSCodingKey.id)
        aCoder.encode(name, forKey: NSCodingKey.name)
        aCoder.encode(phone, forKey: NSCodingKey.phone)
    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let identifier = aDecoder.decodeObject(forKey: NSCodingKey.id) as? String else {
            return nil
        }
        
        self.id = identifier
        self.name = aDecoder.decodeObject(forKey: NSCodingKey.name) as? String
        self.phone = aDecoder.decodeObject(forKey: NSCodingKey.phone) as? String
    }

}
