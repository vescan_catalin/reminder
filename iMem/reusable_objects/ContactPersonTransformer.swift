//
//  Transformer.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation

class ContactPersonTransformer: ValueTransformer {
    override class func transformedValueClass() -> AnyClass {
        return ContactPerson.self
    }
    
    override class func allowsReverseTransformation() -> Bool {
        return true
    }
    
    // convert from my object type to data
    override func transformedValue(_ value: Any?) -> Any? {
        guard let person = value as? ContactPerson else {
            return nil
        }
        return NSKeyedArchiver.archivedData(withRootObject: person)
    }
    
    // convert from data to my object type
    override func reverseTransformedValue(_ value: Any?) -> Any? {
        guard let data = value as? Data else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data)
    }
}
