//
//  DateFormat.swift
//  iMem
//
//  Created by Catalin Vescan on 02/10/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit

extension DateFormatter {
    class func dateFormatter() -> DateFormatter {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.init(forSecondsFromGMT: 0) as TimeZone
        dateFormatter.dateFormat = DateFormat.notificationDateFormat
        
        return dateFormatter
    }
}
