//
//  Location.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation

@objc class MyLocation: NSObject, Codable, NSCoding {
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(NSNumber(value: latitude), forKey: NSCodingKey.latitude)
        aCoder.encode(NSNumber(value: longitude), forKey: NSCodingKey.longitute)
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let lat = aDecoder.decodeObject(forKey: NSCodingKey.latitude) as? NSNumber,
            let long = aDecoder.decodeObject(forKey: NSCodingKey.longitute) as? NSNumber {
            self.latitude = lat.doubleValue
            self.longitude = long.doubleValue
        } 
    }

}

