//
//  MyLocationTransformer.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation

class MyLocationTransformer: ValueTransformer {
    override class func transformedValueClass() -> AnyClass {
        return MyLocation.self
    }
    
    override class func allowsReverseTransformation() -> Bool {
        return true
    }
    
    // convert from my object type to data
    override func transformedValue(_ value: Any?) -> Any? {
        guard let location = value as? MyLocation else {
            return nil
        }
        return NSKeyedArchiver.archivedData(withRootObject: location)
    }
    
    // convert from data to my object type
    override func reverseTransformedValue(_ value: Any?) -> Any? {
        guard let data = value as? Data else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data)
    }
}
