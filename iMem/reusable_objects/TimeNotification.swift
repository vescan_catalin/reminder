//
//  TimeNotification.swift
//  iMem
//
//  Created by Catalin Vescan on 18/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import Foundation

class TimeNotification {
    
    lazy var dateFormatter: DateFormatter = {
        return DateFormatter.dateFormatter()
    }()
    
    var date: NSDate?
    var hour: Int?
    var minute: Int?
    
    func reminderTime() -> NSDate? {
        var timeInterval = DateComponents()
        if let date = self.date, let hour = self.hour, let minute = self.minute {
            timeInterval.hour = -hour
            timeInterval.minute = -minute
            if let reminder = self.dateFormatter.calendar.date(byAdding: timeInterval, to: date as Date) {
                return reminder as NSDate
            }
        }
           
        return NSDate()
    }
}
