//
//  CalendarCell.swift
//  iMem
//
//  Created by Catalin Vescan on 25/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit

class CalendarCell: UICollectionViewCell {

    @IBOutlet weak var calendarTitle: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.calendarTitle.numberOfLines = 3
        self.calendarTitle.lineBreakMode = .byWordWrapping
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.calendarTitle.numberOfLines = 0
        self.calendarTitle.text = nil
        self.layer.borderWidth = 0
        self.layer.borderColor = nil
    }
}
