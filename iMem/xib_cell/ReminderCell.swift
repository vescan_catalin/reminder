//
//  ReminderCell.swift
//  iMem
//
//  Created by Catalin Vescan on 16/09/2019.
//  Copyright © 2019 Alin Petrus. All rights reserved.
//

import UIKit

class ReminderCell: UITableViewCell {

    @IBOutlet weak var reminderName: UILabel!
    @IBOutlet weak var reminderDate: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reminderName.text = nil
        self.reminderDate.text = nil
    }
    
}
